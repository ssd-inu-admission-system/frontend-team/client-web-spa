import React from 'react';

import classes from './SideBar.module.css';
import person from "../../../assets/round.png";
import {NavLink} from "react-router-dom";

const sideBar = props => {
  return (
    <React.Fragment>
      <div className={classes.sideBarPlaceholder} />
      <div className={classes.sideBar}>
        <div className={classes.accountInfo}>
          <img className={classes.userImg} src={person} alt="person" />
          <div className={classes.personInfo}>
            <span className={classes.name}>{props.personName}</span>
            <span className={classes.status}>{props.personStatus}</span>
          </div>
        </div>
        <hr className={classes.divider} />
        <nav className={classes.navigation}>
          <ul className={classes.links}>
            {
              props.menuItems.map(item => (
                <li key={item.link} className={classes.navItem}>
                  <NavLink className={classes.navLink} to={item.link}>{item.title}</NavLink>
                </li>
              ))
            }
          </ul>
        </nav>
        <hr className={classes.divider} />
        <div className={classes.bottomMenu}>
          <NavLink disabled className={classes.navLink} to={props.settingsLink}>Settings</NavLink>
          <button className={classes.logoutButton} onClick={props.logoutHandler}>Log out</button>
        </div>
      </div>
    </React.Fragment>
  );
};

export default sideBar;
