import * as api from '../../services/apiConnector';
import * as types from './actionTypes';

import * as actionCenter from '../actionCenter/actions';

import {getApplicationID, getUserToken} from "../user/reducer";

export function fetchTestsFromServer() {
    return async (dispatch, getState) => {
        dispatch(actionCenter.startAction("fetching tests"));

        const resp = await api.getTests(getUserToken(getState()));
        if (resp.status === 0) {

            dispatch({
                type: types.TESTS_FETCHED,
                tests: resp.tests
            });
            dispatch(actionCenter.finishAction("fetching tests"));
        } else {
            dispatch(actionCenter.failAction("fetching tests", resp.error_message));
        }
    }
}

export function fetchTestsResultsFromServer() {
    return async (dispatch, getState) => {
        dispatch(actionCenter.startAction("fetching tests"));

        const resp = await api.getTestsResults(getApplicationID(getState()), getUserToken(getState()));
        if (resp.status === 0) {
            dispatch({
                type: types.TESTS_RESULTS_FETCHED,
                testResults: resp.results
            });
            dispatch(actionCenter.finishAction("fetching tests"));
        } else {
            dispatch(actionCenter.failAction("fetching tests", resp.error_message));
        }
    }
}

export function fetchTestInfoFromServer(id) {
    return async (dispatch, getState) => {
        dispatch(actionCenter.startAction("fetching test info"));

        const resp = await api.getTestInfo(getUserToken(getState()), id);
        if (resp.status === 0) {
            dispatch({
                type: types.TEST_INFO_FETCHED,
                testInfo: {
                    test_id: id,
                    title: resp.title,
                    program_id: resp.program_id,
                    questions: resp.questions
                }
            });
            dispatch(actionCenter.finishAction("fetching test info"));
        } else {
            dispatch(actionCenter.failAction("fetching test info", resp.error_message));
        }
    }
}

export function sendTestSolution(testSolution) {
    return async (dispatch, getState) => {
        dispatch(actionCenter.startAction("sending test solution"));

        console.log('sendTestSolution', testSolution, getApplicationID(getState()), getUserToken(getState()) ? 'token' : 'not token');

        const resp = await api.sendTestSolutions(testSolution, getApplicationID(getState()), getUserToken(getState()));
        if (resp.status === 0) {
            dispatch({
                type: types.TEST_INFO_FETCHED,
                testResults: resp.results
            });
            dispatch(actionCenter.finishAction("sending test solution", "Test successfully submitted"));
            setTimeout(() => dispatch(actionCenter.popMessage()), 3000);
        } else {
            dispatch(actionCenter.failAction("sending test solution", resp.error_message));
        }
    }
}
