import React from 'react';
import axios from 'axios';
import {ApplicationFormWrapper} from '../../components/ApplicationFormWrapper/applicationFormWrapper';

import {DocumentsForm} from "../../components/DocumentsForm/documentsForm";
import {BASE_URL} from "../../services/apiConnector";
import {getApplicationID, getDocuments, getFullName, getUserId, getUserToken} from "../../store/user/reducer";
import {connect} from "react-redux";
import {failAction, finishAction, startAction} from "../../store/actionCenter/actions";
import {fetchApplication, fetchUserData} from "../../store/user/actions";
import {withRouter} from "react-router-dom";

class DocumentsPage extends React.Component {
  componentDidMount() {
    fetchUserData();
    const checkDocs = () => {
      if (!this.props.applicationId) {
        setTimeout(checkDocs, 100);
      } else {
        if (this.props.documents == null) this.props.dispatch(fetchApplication());
      }
    };
    checkDocs();
  }

  sendDocument = async (document, type) => {
    const url = BASE_URL + `/documents/${this.props.applicationId}/send?token=${this.props.token}`;
    const formData = new FormData();
    formData.append('type', type);
    formData.append('document', document, document.name);

    this.props.dispatch(startAction('sending file'));
    const response = await axios.post(url, formData);
    if (response.data.status !== 0) {
      this.props.dispatch(failAction('sending file', `Cannot upload file. ${response.error_message}`));
    } else {
      this.props.dispatch(fetchApplication());
      this.props.dispatch(finishAction('sending file', 'File has been successfully uploaded'));
    }
  };

  render() {
    return (
      <ApplicationFormWrapper
        userName={this.props.fullName}
        userStatus={'ID: ' + this.props.userId}
      >
        <DocumentsForm
          onDocumentSend={this.sendDocument}
          documentsList={this.props.documents ? this.props.documents : []}
          token={this.props.token}
        />
      </ApplicationFormWrapper>
    );
  }
}

const mapStateToProps = state => {
  return {
    fullName: getFullName(state),
    userId: getUserId(state),
    applicationId: getApplicationID(state),
    token: getUserToken(state),
    documents: getDocuments(state)
  };
};

export default withRouter(connect(mapStateToProps)(DocumentsPage));
