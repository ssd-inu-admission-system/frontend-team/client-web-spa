import React, {Component} from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';

import LoginPage from './containers/LoginPage/loginPage'
import PersonalDataPage from './containers/PersonalDataPage/personalDataPage'
import EducationDataPage from './containers/EducationDataPage/educationDataPage'
import Spinner from './components/Spinner/Spinner';

import {getUserData, getUserToken} from "./store/user/reducer";
import {fetchTokenFromCookies} from "./store/user/actions";
import {getFirstError, getFirstMessage, isActionInProgress} from "./store/actionCenter/reducer";
import Snackbar from "./components/Snackbar/Snackbar";
import {popError, popMessage} from "./store/actionCenter/actions";
import PassTestPage from './containers/PassTestPage/passTestPage';
import TestsListPage from "./containers/TestListPage/testListPage";
import DocumentsPage from './containers/DocumentsPage/documentPage';
import ApplicationsDetails from './containers/ApplicationsDetails/applicationsDetails'

class App extends Component {
    constructor(props) {
        super(props);
        props.dispatch(fetchTokenFromCookies());
    }

  render() {
    return (
      <React.Fragment>
        {this.props.actionInProgress ? (<Spinner/>) : ''}
        {this.props.token && this.props.userData.email ? (
          <Switch>
            <Route path="/" exact component={PersonalDataPage}/>
              <Route path="/education_data" component={EducationDataPage}/>
              <Route path="/pass_test/:id" component={PassTestPage}/>
              <Route path="/application" component={ApplicationsDetails}/>
              <Route path="/tests" component={TestsListPage}/>
              <Route path="/docs" component={DocumentsPage}/>
              <Redirect from="*" to="/"/>
            </Switch>
        ) : (
          <LoginPage/>
        )}
        {this.props.error ? (<Snackbar
          type={"error"}
          onClickHandler={() => this.props.dispatch(popError())}
        >
          {this.props.error}
        </Snackbar>) : ''}
        {this.props.message && !this.props.error ? (<Snackbar
          type={"message"}
          onClickHandler={() => this.props.dispatch(popMessage())}
        >
          {this.props.message}
        </Snackbar>) : ''}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: getUserToken(state),
    actionInProgress: isActionInProgress(state),
    error: getFirstError(state),
    message: getFirstMessage(state),
  userData: getUserData(state)
    }
};

export default connect(mapStateToProps)(App);
