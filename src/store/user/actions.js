import * as Cookies from 'js-cookie';

import * as api from '../../services/apiConnector';
import * as types from './actionTypes';

import * as selectors from './reducer';

import * as actionCenter from '../actionCenter/actions';
import {getUserToken} from "./reducer";
import {getUserId} from "./reducer";

export function fetchTokenFromCookies() {
  return (dispatch, getState) => {
    let data = Cookies.get('user-data');
    if (typeof(data) == 'string') {
      data = JSON.parse(data);
      dispatch({
        type: types.TOKEN_FETCHED,
        ...data
      });
      dispatch(fetchUserData());
    }
  }
}

export function fetchTokenFromServer(email, password) {
  return async(dispatch, getState) => {
    // Start fetching process
    dispatch(actionCenter.startAction("token_fetching"));

    // Send info to the server
    const resp = await api.signIn(email, password);
    if (resp.status === 0) {
      const data = {
        token: resp.token,
        id: resp.id,
        firstName: resp.first_name,
        lastName: resp.last_name
      };

      // Place data in cookies
      Cookies.set('user-data', JSON.stringify(data));

      // Resolve fetching process
      dispatch(actionCenter.finishAction("token_fetching"));
      dispatch({
        type: types.TOKEN_FETCHED,
        ...data
      });
      dispatch(fetchUserData());
    } else {
      dispatch(actionCenter.failAction("token_fetching", resp.error_message));
    }
  }
}

// Required data: object that includes following fields:
// - email
// - password
// - firstName
// - lastName
// - phone
// - citizenship
// - source
export function registerNewAccout(accountData) {
  return async(dispatch, getState) => {
    // Start the process
    dispatch(actionCenter.startAction("signing_up"));

    // Send info to the server
    const resp = await api.signUp(accountData);
    if (resp.status === 0) {
      const data = {
        token: resp.token,
        id: resp.id,
        firstName: accountData.firstName,
        lastName: accountData.lastName,
      };

      // Place data in cookies
      Cookies.set('user-data', JSON.stringify(data));
      dispatch({
        type: types.SIGNED_UP,
        ...data
      });


      // creating new application
      dispatch(actionCenter.startAction("creating default application id"));

      const anotherResp = await api.createApplication(getUserToken(getState()), getUserId(getState()));
      if (anotherResp.status === 0) {
        dispatch({
          type: types.APPLICATION_CREATED,
          application_id: anotherResp.id // this is application id
        });
        dispatch(actionCenter.finishAction("creating default application id"));
      } else {
        dispatch(actionCenter.failAction("creating default application id", anotherResp.error_message));
      }


      dispatch(fetchUserData());
      dispatch(actionCenter.finishAction("signing_up"));
    } else {
      dispatch(actionCenter.failAction("signing_up", resp.error_message));
    }
  }
}

export function fetchUserData() {
  return async(dispatch, getState) => {
    // Start process
    dispatch(actionCenter.startAction("fetching_user_data"));

    const token = selectors.getUserToken(getState());
    const id = selectors.getUserId(getState());

    const resp = await api.getFullUserInfo(token, id);

    if (resp.status === 0) {
      const data = {
        email: resp.email,
        firstName: resp.first_name,
        lastName: resp.last_name,
        phone: resp.phone,
        citizenship: resp.citizenship,
        source: resp.source,
        birthDate: resp.birth_date,
        gender: resp.gender,
        country: resp.country,
        city: resp.city,
        middleName: resp.middle_name,
        schoolCountry: resp.school_country,
        schoolCity: resp.school_city,
        schoolName: resp.school_name,
        graduationYear: resp.graduation_year,
        gradeAverage: resp.grade_average,
        yearRussian: resp.year_russian,
        gradeRussian: resp.grade_russian,
        yearMath: resp.year_math,
        gradeMath: resp.grade_math,
        yearInfoPhysics: resp.year_info_physics,
        gradeInfoPhysics: resp.grade_info_physics,
        dateDiploma: resp.date_diploma,
        applications: resp.applications
      };

      setTimeout(() => {
        dispatch(actionCenter.finishAction("fetching_user_data"));
        dispatch({
          type: types.USER_INFO_FETCHED,
          data
        });
      }, 500);
    } else {
      dispatch(actionCenter.failAction("fetching_user_data", resp.error_message));
    }
  }
}

// Required data: object that includes following fields:
// “email”: string,
// “firstName”: string,
// “lastName”: string,
// “phone”: string,
// “citizenship”: string,
// “source”: string,
// ”birthDate”: date,
// “gender”: "male"|"female",
// “country”: string,
// “city”: string,
// (optional)”middleName”: string,
export function updatePersonalInfo(data) {
  return async(dispatch, getState) => {
    dispatch(actionCenter.startAction("updating_personal_data"));

    const token = selectors.getUserToken(getState());
    const id = selectors.getUserId(getState());

    const resp = await api.updatePersonalInfo(token, id, data);

    if (resp.status === 0) {
      dispatch(actionCenter.finishAction("updating_personal_data", "Successfully updated"));
      setTimeout(() => dispatch(actionCenter.popMessage()), 3000);
      dispatch({
        type: types.PERSONAL_INFO_UPDATED,
        data
      });
    } else {
      dispatch(actionCenter.failAction("updating_personal_data", resp.error_message));
    }
  }
}

// Required data: object that includes following fields:
// “schoolCountry”: string,
// “schoolCity”: string,
// “schoolName”: string,
// “graduationYear”: int,
// (optional)”gradeAverage”: float,
// (optional)”yearRussian”: int,
// (optional)”gradeRussian”: int,
// (optional)”yearMath”: int,
// (optional)”gradeMath”: int,
// (optional)”yearInfoPhysics”: int,
// (optional)”gradeInfoPhysics”: int,
// (optional)”dateDiploma”: date
export function updateEducationInfo(data) {
  return async(dispatch, getState) => {
    dispatch(actionCenter.startAction("updating_educational_info"));

    const token = selectors.getUserToken(getState());
    const id = selectors.getUserId(getState());

    const resp = await api.updateEducationInfo(token, id, data);

    if (resp.status === 0) {
      dispatch(actionCenter.finishAction("updating_educational_info", "Successfully updated"));
      setTimeout(() => dispatch(actionCenter.popMessage()), 3000);
      dispatch({
        type: types.EDUCATION_INFO_UPDATED,
        data
      });
    } else {
      dispatch(actionCenter.failAction("updating_educational_info", resp.error_message));
    }
  }
}

export function fetchApplication() {
  return async(dispatch, getState) => {
    dispatch(actionCenter.startAction("fetchApplication"));

    const token = selectors.getUserToken(getState());
    const id = selectors.getApplicationID(getState());

    const resp = await api.getApplicationInfo(token, id);

    if (resp.status === 0) {
      dispatch({
        type: types.APPLICATION_FETCHED,
        applicationData: resp
      });
      dispatch(actionCenter.finishAction("fetchApplication"));
    } else {
      dispatch(actionCenter.failAction("fetchApplication", resp.error_message));
    }
  }
}

export function invalidateToken() {
  return (dispatch, getState) => {
    Cookies.remove('user-data');
    dispatch({type: types.TOKEN_INVALIDATED});
  }
}
