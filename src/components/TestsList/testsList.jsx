import React from "react";
import {NavLink} from 'react-router-dom'

import styles from "./testsList.module.css";

export const TestsList = (props) => {
    const testResults = {};
    if (props.testResults) {
        console.log(props.testResults);
        props.testResults.forEach((testResult) => {
            testResults[testResult.test_id] = testResult.grade;
        })
    }

    return (
        <div>
            {props.testsList.map((test) => {
                return (
                    <div className={styles.container} key={test.test_id}>
                        <div className={styles.flexContainer}>
                            <h3>{test.title || `test title of ${test.test_id} test`}</h3>
                            <span className={styles.flexEndContainer}>
                                {testResults[test.test_id] ? `Result: ${testResults[test.test_id]}` : "not passed"}
                            </span>
                        </div>
                        <span>
                            {test.description || `test description of ${test.test_id} test`}
                        </span>
                        <NavLink to={`/pass_test/${test.test_id}`} className={styles.link}>
                            <div className={styles.buttonTest}>
                                Pass test
                            </div>
                        </NavLink>
                    </div>
                );
            })}
        </div>
    );
};
