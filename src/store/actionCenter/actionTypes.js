export const ACTION_STARTED = 'actionCenter.ACTION_STARTED';
export const ACTION_FAILED = 'actionCenter.ACTION_FAILED';
export const ACTION_FINISHED = 'actionCenter.ACTION_FINISHED';
export const ERROR_PUSHED = 'actionCenter.ERROR_PUSHED';
export const ERROR_POPPED = 'actionCenter.ERROR_POPPED';
export const MESSAGE_PUSHED = 'actionCenter.MESSAGE_PUSHED';
export const MESSAGE_POPPED = 'actionCenter.MESSAGE_POPPED';
