import React from "react";

import styles from "./personalDataForm.module.css";
import InputMask from 'react-input-mask';

export const PersonalDataForm = (props) => {
    return (
        <form className={styles.formContainer} onSubmit={props.onSubmit}>
            <p className={styles.personalDataText}>Personal data</p>
            <TextField title="First Name"
                       label="firstName"
                       data={props.data}
                       handleChange={props.handleChange}/>
            <TextField title="Last Name"
                       label="lastName"
                       data={props.data}
                       handleChange={props.handleChange}/>
            <TextField title="Middle Name (optional)"
                       label="middleName"
                       data={props.data}
                       handleChange={props.handleChange}/>
            <TextField title="Date of Birth"
                       label="birthDate"
                       type="date"
                       data={props.data}
                       handleChange={props.handleChange}/>
            <div>
                <p className={styles.textFieldTitle}>Gender</p>
                <select className={styles.textFieldInput}
                        placeholder={"Gender"}
                        onChange={props.handleChange("gender")}
                        value={props.data.gender}>
                    <option></option>
                    <option value={"male"}>Male</option>
                    <option value={"female"}>Female</option>
                </select>
            </div>
            <TextField title="Email"
                       label="email"
                       type="email"
                       data={props.data}
                       handleChange={props.handleChange}/>
            <div>
                <p className={styles.textFieldTitle}>Phone</p>
                <InputMask className={styles.textFieldInput}
                           mask="9-(999)-999-99-99"
                           placeholder={"Phone"}
                           value={props.data["phone"] || ""}
                           onChange={props.handleChange("phone")}/>
            </div>
            <TextField title="Citizenship"
                       label="citizenship"
                       data={props.data}
                       handleChange={props.handleChange}/>
            <TextField title="Country of living"
                       label="country"
                       data={props.data}
                       handleChange={props.handleChange}/>
            <TextField title="City"
                       label="city"
                       data={props.data}
                       handleChange={props.handleChange}/>
            <TextField title="Where did you know about us?"
                       label="source"
                       data={props.data}
                       handleChange={props.handleChange}/>
            <input className={styles.nextSectionButton}
                   type={"submit"}
                   value={"Next: Education"}/>
        </form>
    );
};


const TextField = (props) => {
    return (
        <div>
            <p className={styles.textFieldTitle}>{props.title}</p>
            <input className={styles.textFieldInput}
                   type={props.type || "text"}
                   placeholder={props.title}
                   value={props.data[props.label] || ""}
                   onChange={props.handleChange(props.label)}/>
        </div>
    );
};
