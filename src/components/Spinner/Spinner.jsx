import React from "react";

import "./Spinner.css";

const spinner = () => (
  <div className="spinner-container">
    <div className="nb-spinner" />
  </div>
);

export default spinner;
