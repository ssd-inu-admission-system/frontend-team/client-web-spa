import React from 'react';


import {connect} from "react-redux";
import {fetchApplication} from "../../store/user/actions";
import {ApplicationWrapper} from "../../components/ApplicationWrapper/applicationWrapper";
import {LoadingWidget} from "../../components/LoadingWIdget/loadingWidget";
import {
    getApplicationData,
    getApplicationID,
    getApplicationStatus,
    getFullName,
    getUserData, getUserId
} from "../../store/user/reducer";
import {PersonalData, EducationData, Documents, Tests} from './applicationDetailsInfo';
import InterviewerComment from '../../components/InterviewerComment/interviewerComment'

import styles from './applicationsDetails.module.css';
import {Accordion, AccordionMenu, Panel} from '../../components/Accordion/accordion';
import Layout from "../../components/Layout/Layout";
import {fetchTestsFromServer, fetchTestsResultsFromServer} from "../../store/tests/actions";

class ApplicationsDetails extends React.Component {

    information = [
        {
            component: PersonalData,
            title: "Personal Data"
        },
        {
            component: EducationData,
            title: "Education"
        },
    ];

    state = {
        isDialogActive: false,
    };

    constructor(props) {
        super(props);
        if (props.applicationID) {
            props.dispatch(fetchApplication());
        }
    }

    render() {
        if (!this.props.currentApplication) {
            return (
                <Layout
                    userName={this.props.userName}
                    userStatus={"ID: " + this.props.userId}>
                    <ApplicationWrapper headLine={`Application ID ${this.props.applicationID}`}>
                        <LoadingWidget/>
                    </ApplicationWrapper>
                </Layout>
            );
        }
        return (
            <Layout
                userName={this.props.userName}
                userStatus={"ID: " + this.props.userId}>
                <ApplicationWrapper
                    headLine={
                        `Application ID ${this.props.applicationID}, ${
                            this.getStatus(this.props.applicationStatus
                            )}`
                    }>
                    {this.information.map((data) => {
                        return (
                            <div key={data.title}>
                                <AccordionMenu>
                                    <Accordion title={data.title} className={styles.titleBox}>
                                        <Panel>
                                            <hr/>
                                            <div className={styles.accordionBody} onClick={event => event.stopPropagation()}>
                                                {data.component(this.props)}
                                            </div>
                                        </Panel>
                                    </Accordion>
                                </AccordionMenu>
                            </div>

                        )
                    })}
                    <InterviewerComment
                        comment={this.props.currentApplication.comment}
                        grade={this.props.currentApplication.grade}
                        applicationInterviewer={this.props.currentApplication.interviewer_id}
                        applicationID={this.props.match.params.id}/>
                </ApplicationWrapper>
            </Layout>
        );
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.applicationID && !nextProps.currentApplication) {
            nextProps.dispatch(fetchApplication());
        }
    }

    getStatus(code) {
        switch (code) {
            case 0:
                return 'applied';
            case 1:
                return 'on interview';
            case 2:
                return 'approved';
            case 3:
                return 'rejected';
            default:
                return 'all';
        }
    }
}

const mapStateToProps = (state) => {
    return {
        currentApplication: getApplicationData(state),
        applicationID: getApplicationID(state),
        userData: getUserData(state),
        applicationStatus: getApplicationStatus(state),
        userName: getFullName(state),
        userId: getUserId(state),
    }
};

export default connect(mapStateToProps)(ApplicationsDetails);
