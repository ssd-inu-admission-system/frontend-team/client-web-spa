import React from 'react';
import {ApplicationWrapper} from '../../components/ApplicationWrapper/applicationWrapper';
import {TestsList} from '../../components/TestsList/testsList';
import {connect} from "react-redux";
import {LoadingWidget} from "../../components/LoadingWIdget/loadingWidget";
import {getTestResults, getTests} from "../../store/tests/reducer";
import {fetchTestsFromServer, fetchTestsResultsFromServer} from "../../store/tests/actions";
import Layout from "../../components/Layout/Layout";
import {getApplicationID, getFullName, getUserData, getUserId, getUserToken} from "../../store/user/reducer";

class TestsListPage extends React.Component {

    constructor(props) {
        super(props);
        if (props.applicationID) {
            props.dispatch(fetchTestsFromServer());
            props.dispatch(fetchTestsResultsFromServer());
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.applicationID && !nextProps.testsList) {
            nextProps.dispatch(fetchTestsFromServer());
        }

        if (nextProps.applicationID && !nextProps.testResults) {
            nextProps.dispatch(fetchTestsResultsFromServer());
        }
    }

    render() {
        if (!this.props.testsList || !this.props.testResults) {
            return (
                <Layout>
                    <LoadingWidget/>
                </Layout>);
        }
        return (
            <Layout
                userName={this.props.userName}
                userStatus={"ID: " + this.props.userId}>
                <ApplicationWrapper headLine="Tests"
                                    userName={this.props.userName}
                                    userStatus={"ID: " + this.props.userId}>
                    <TestsList testsList={this.props.testsList} testResults={this.props.testResults}/>
                </ApplicationWrapper>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        testsList: getTests(state),
        userData: getUserData(state),
        userName: getFullName(state),
        userId: getUserId(state),
        testResults: getTestResults(state),
        applicationID: getApplicationID(state),
    }
};

export default connect(mapStateToProps)(TestsListPage);