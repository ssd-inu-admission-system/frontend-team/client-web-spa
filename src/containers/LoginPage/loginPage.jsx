import React from 'react';
import {connect} from 'react-redux';

import {SignUpForm} from '../../components/SignUpForm/signUpForm';
import {StaticData} from '../../components/LoginPageStaticData/loginPageStaticData';
import {SignInForm} from '../../components/SignInForm/signInForm'

import * as actions from '../../store/user/actions';

import styles from './loginPage.module.css';

class LoginPage extends React.Component {

    state = {
        signUpForm: {},
        signInForm: {},
        currentSignUp: true,
    };

    handleSignUpChange = (key) => (event) => {
        const newState = {
            ...this.state,
            signUpForm: {
                ...this.state.signUpForm
            }
        };
        newState.signUpForm[key] = event.target.value;

        this.setState(newState);
    };

    handleSignInChange = (key) => (event) => {
        const newState = {
            ...this.state,
            signInForm: {
                ...this.state.signInForm
            }
        };
        newState.signInForm[key] = event.target.value;

        this.setState(newState);
    };

    onSignUpSubmit = (event) => {
        event.preventDefault();
        this.props.dispatch(actions.registerNewAccout(this.state.signUpForm));
    };

    onSignInSubmit = (event) => {
        event.preventDefault();
        this.props.dispatch(
          actions.fetchTokenFromServer(this.state.signInForm.email,
                                        this.state.signInForm.password)
        );
    };

    changeForm = () => {
        const newState = {
            ...this.state
        };
        newState.currentSignUp = !newState.currentSignUp;
        this.setState(newState);
    };

    getCurrentForm = () => {
        if (this.state.currentSignUp) {
            return (
                <SignUpForm data={this.state.signUpForm}
                            handleChange={this.handleSignUpChange}
                            onSubmit={this.onSignUpSubmit}
                            onChangeForm={this.changeForm}/>
            );
        } else {
            return (
                <SignInForm data={this.state.signInForm}
                            handleChange={this.handleSignInChange}
                            onSubmit={this.onSignInSubmit}
                            onChangeForm={this.changeForm}/>
            );
        }
    };

    render() {
        return (
            <div className={styles.mainContainer}>
                <div className={styles.mainContainerBlur}>
                    <div className={styles.firstChild}>
                        <StaticData/>
                    </div>
                    {this.getCurrentForm()}
                </div>
            </div>
        )
    }
}

export default connect()(LoginPage);
