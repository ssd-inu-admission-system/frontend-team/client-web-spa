import React from 'react';

import styles from './interviewerComment.module.css';


export default class InterviewerComment extends React.Component {

    state = {
        comment: undefined,
        grade: undefined,
    };

    constructor(props) {
        super(props);
        this.state.comment = props.comment;
        this.state.grade = props.grade;
    }

    render() {
        const enabled = false;
        return (
            <div>
                <div className={styles.text}>Grade to the application (from 0 to 100):</div>
                <input
                    className={styles.inputTextFiled}
                    value={this.state.grade || ""}
                    disabled={!enabled}/>
                <div className={styles.text}>Comment from interviewer:</div>
                <textarea
                    rows={4}
                    className={styles.inputTextFiled}
                    value={this.state.comment || ""}
                    disabled={!enabled}/>
            </div>
        );
    }
}
