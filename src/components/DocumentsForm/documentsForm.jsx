import React, {useCallback} from "react";
import doc from "../../assets/doc.png";
import styles from "./documentsForm.module.css";
import {useDropzone} from 'react-dropzone';
import {BASE_URL} from "../../services/apiConnector";

export const DocumentsForm = (props) => {
  return (
    <div>
      <div>
        <p className={styles.status}>Obligatory</p>
        <div className={styles.documents}>
          <Document
            documentName="CV/Portfolio"
            sendDocument={(document) => props.onDocumentSend(document, 1)}
            file={props.documentsList.filter(doc => doc.document_type === 1)[0]}
            token={props.token}
          />
          <Document
            documentName="Motivation Letter"
            sendDocument={(document) => props.onDocumentSend(document, 2)}
            file={props.documentsList.filter(doc => doc.document_type === 2)[0]}
            token={props.token}
          />
          <Document
            documentName="Transcripts"
            sendDocument={(document) => props.onDocumentSend(document, 4)}
            file={props.documentsList.filter(doc => doc.document_type === 4)[0]}
            token={props.token}
          />
          <Document
            documentName="Scan of Passport"
            sendDocument={(document) => props.onDocumentSend(document, 3)}
            file={props.documentsList.filter(doc => doc.document_type === 3)[0]}
            token={props.token}
          />
        </div>
      </div>
      <div >
        <p className={styles.status}>Optional</p>
        <div className={styles.documents}>
          <Document
            documentName="Recommendations"
            sendDocument={(document) => props.onDocumentSend(document, 5)}
            file={props.documentsList.filter(doc => doc.document_type === 5)[0]}
            token={props.token}
          />
          <Document
            documentName="Project Description"
            sendDocument={(document) => props.onDocumentSend(document, 6)}
            file={props.documentsList.filter(doc => doc.document_type === 6)[0]}
            token={props.token}
          />
        </div>
      </div>
    </div>
  );
};

const Document = props => {

  if (props.file) {
    return (
      <div className={styles.document} onClick={() => window.open(`${BASE_URL}\\${props.file.document_url}?token=${props.token}`)}>
        <h3>{props.documentName}</h3>
        <img src={doc} alt={'document'}/>
        <p>Click to Download</p>
      </div>
    )
  } else {
    const onDrop = useCallback(acceptedFiles => {
      props.sendDocument(acceptedFiles[0]);
    }, []);
    const {getRootProps, getInputProps} = useDropzone({
      multiple: false,
      accept: [
        '.pdf',
        '.txt',
        '.doc',
        '.docx'
      ],
      onDrop
    });

    return (
      <div {...getRootProps({className: styles.document})}>
        <input {...getInputProps()} />
        <h3>{props.documentName}</h3>
        <img src={doc} alt={'document'}/>
        <p>{'Choose file...'}</p>
      </div>
    );
  }
};
