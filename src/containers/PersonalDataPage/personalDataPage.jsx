import React from 'react';
import {connect} from 'react-redux';
import {Redirect, withRouter} from 'react-router-dom';
import {ApplicationFormWrapper} from '../../components/ApplicationFormWrapper/applicationFormWrapper';
import {PersonalDataForm} from "../../components/PersonalDataForm/personalDataForm";
import {getFullName, getUserId, getUserData, getUserToken} from "../../store/user/reducer";
import {updatePersonalInfo} from "../../store/user/actions";
import {getFirstError, isActionInProgress} from "../../store/actionCenter/reducer";

class PersonalDataPage extends React.Component {
  componentDidMount() {
    let updateState = () => {
      if (this.props.actionInProgress) {
        setTimeout(updateState, 100);
      } else {
        this.setState({
          personalData: {
            email: this.props.userData.email,
            firstName: this.props.userData.firstName,
            lastName: this.props.userData.lastName,
            phone: this.props.userData.phone,
            citizenship: this.props.userData.citizenship,
            source: this.props.userData.source,
            birthDate: this.props.userData.birthDate,
            gender: this.props.userData.gender ? "male" : "female",
            country: this.props.userData.country,
            city: this.props.userData.city,
            middleName: this.props.userData.middleName,
          }
        });
      }
    };
    updateState();
  }

  state = {personalData: {}};
  handleChange = (key) => (event) => {
    const newState = {...this.state, personalData: {...this.state.personalData}};
    newState.personalData[key] = event.target.value;
    this.setState(newState);
  };
  onSubmit = (event) => {
    event.preventDefault();
    this.props.dispatch(updatePersonalInfo(this.state.personalData)).then(() => {
      if (!this.props.error) {
        this.props.history.push('/education_data');
      }
    });
  };

  render() {
    if (!this.props.token) return (<Redirect to="/"/>);
    return (
      <ApplicationFormWrapper userName={this.props.fullName} userStatus={'ID: ' + this.props.userId}> <PersonalDataForm
        data={this.state.personalData} handleChange={this.handleChange} onSubmit={this.onSubmit}/>
      </ApplicationFormWrapper>)
  }
}

const mapStateToProps = state => {
  return {
    fullName: getFullName(state),
    userId: getUserId(state),
    userData: getUserData(state),
    actionInProgress: isActionInProgress(state),
    error: getFirstError(state),
    token: getUserToken(state)
  }
};
export default withRouter(connect(mapStateToProps)(PersonalDataPage));

