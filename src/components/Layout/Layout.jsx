import React from 'react';
import styles from './Layout.module.css';

import TopBar from './TopBar/TopBar';
import SideBar from './SideBar/SideBar';
import {connect} from "react-redux";
import {invalidateToken} from "../../store/user/actions";

const Layout = (props) => {
    const menuItems = [
        {
            title: "My Information",
            link: "/"
        },
        {
            title: "Documents",
            link: "/docs"
        },
        {
            title: "Tests",
            link: "/tests"
        },
        {
            title: "Application",
            link: "/application"
        }
    ];

    return (
        <div className={styles.container}>
            <SideBar
                menuItems={menuItems}
                logoutHandler={() => props.dispatch(invalidateToken())}
                settingsLink="/"
                personName={props.userName}
                personStatus={props.userStatus}
            />
            <div className={styles.mainPart}>
                <TopBar/>
                <div className={styles.content}>
                    {props.children}
                </div>
            </div>
        </div>
    )
};

export default connect()(Layout); // ToDo refactor here
