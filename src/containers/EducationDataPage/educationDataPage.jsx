import React from 'react';
import {EducationDataForm} from "../../components/EducationDataForm/educationDataForm";
import {ApplicationFormWrapper} from '../../components/ApplicationFormWrapper/applicationFormWrapper';
import styles from './educationDataPage.module.css';
import {connect} from "react-redux";
import {getFullName, getUserData, getUserId, getUserToken} from "../../store/user/reducer";
import {Redirect} from "react-router-dom";
import {updateEducationInfo} from "../../store/user/actions";
import {isActionInProgress} from "../../store/actionCenter/reducer";

class EducationDataPage extends React.Component {
  componentDidMount() {
    let updateState = () => {
      if (this.props.actionInProgress) {
        setTimeout(updateState, 100);
      } else {
        this.setState({
          educationData: {
            schoolCountry: this.props.userData.schoolCountry,
            schoolCity: this.props.userData.schoolCity,
            schoolName: this.props.userData.schoolName,
            graduationYear: this.props.userData.graduationYear,
            gradeAverage: this.props.userData.gradeAverage,
            yearRussian: this.props.userData.yearRussian,
            gradeRussian: this.props.userData.gradeRussian,
            yearMath: this.props.userData.yearMath,
            gradeMath: this.props.userData.gradeMath,
            yearInfoPhysics: this.props.userData.yearInfoPhysics,
            gradeInfoPhysics: this.props.userData.gradeInfoPhysics,
            dateDiploma: this.props.userData.dateDiploma,
          }
        });
      }
    };
    updateState();
  }

  state = {educationData: {}};
  handleChange = (key) => (event) => {
    const newState = {...this.state, educationData: {...this.state.educationData}};
    newState.educationData[key] = event.target.value;
    this.setState(newState);
  };
  onSubmit = (event) => {
    event.preventDefault();
    this.props.dispatch(updateEducationInfo(this.state.educationData));
  };

  render() {
    if (!this.props.token) return (<Redirect to="/"/>);
    return (
      <ApplicationFormWrapper userName={this.props.userName} userStatus={"ID: " + this.props.userId}> <EducationDataForm
        data={this.state.educationData} handleChange={this.handleChange} onSubmit={this.onSubmit}/>
      </ApplicationFormWrapper>)
  }
}

const mapStateToProps = state => {
  return {
    token: getUserToken(state),
    userData: getUserData(state),
    userName: getFullName(state),
    userId: getUserId(state),
    actionInProgress: isActionInProgress(state)
  };
};
export default connect(mapStateToProps)(EducationDataPage);

