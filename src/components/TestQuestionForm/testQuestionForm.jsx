import React from "react";

import styles from "./testQuestionForm.module.css";

/**
 question - object what describes question
 {
    text: "",
    type: 1/2, //1- single; 2 - multiple choice
    options: [
        {
            "text": string,
            "is_correct": bool
        }
    ]
 }
 onQuestionChange("text" or "type", event.target.newValue)
 onQuestionOptionChange(optionID, "text" or "is_correct", event.target.newValue)
 */


export class TestQuestionForm extends React.Component {

    onOptionChangeCreator = (index, key) => (event) => {
        let newOptions = [...this.props.question.options];
        switch (key) {
            case "chosen":
                if (this.props.question.type === 1) {
                    newOptions = this.props.question.options.map((object) => {
                        return {text: object.text, chosen: false};
                    });
                }
                newOptions[index][key] = !newOptions[index][key];
                break;
        }
        this.props.onQuestionChange("options", newOptions);
    };

    render() {
        if (this.props.question.type === 0) {
            return (
                <div className={styles.questionBox}>
                    Unsupported question type
                </div>);
        }
        const type = this.props.question.type === 1 ? 'radio' : 'checkbox';
        return (
            <div className={styles.container}>
                <div className={styles.questionBox}>
                    <span className={styles.questionName}>{this.props.question.text}</span>
                    <form>
                        {this.props.question.options.map((option, index) => {
                            return (
                                <div key={index} className={styles.questionName}>
                                    <input type={type} className={styles.questionOption}
                                           checked={this.props.question.options[index].chosen || false}
                                           onChange={this.onOptionChangeCreator(index, "chosen")}/>
                                    {this.props.question.options[index].text}
                                </div>
                            );
                        })}
                    </form>
                </div>
            </div>
        );
    }
}
