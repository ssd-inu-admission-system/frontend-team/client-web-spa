import React from "react";

import styles from "./educationDataForm.module.css";
import InputMask from 'react-input-mask';

export const EducationDataForm = (props) => {
    return (
        <form className={styles.formContainer} onSubmit={props.onSubmit}>
            <p className={styles.personalDataText}>Education</p>
            <TextField title="Country"
                       label="schoolCountry"
                       data={props.data}
                       handleChange={props.handleChange}/>
            <TextField title="City of study"
                       label="schoolCity"
                       data={props.data}
                       handleChange={props.handleChange}/>
            <TextField title="Name of School"
                       label="schoolName"
                       data={props.data}
                       handleChange={props.handleChange}/>
            <NumberField
                title="Graduation Year (actual or expected)"
                mask="9999"
                label="graduationYear"
                placeholder="yyyy"
                data={props.data}
                handleChange={props.handleChange}/>
            <NumberField
                title="Average Grade (optional)"
                mask="9.9"
                label="gradeAverage"
                placeholder="0.0"
                data={props.data}
                handleChange={props.handleChange}/>
            <NumberField
                title="Russian Test Year (optional)"
                mask="9999"
                label="yearRussian"
                placeholder="yyyy"
                data={props.data}
                handleChange={props.handleChange}/>
            <NumberField
                title="Russian Test Result (optional)"
                mask="999"
                label="gradeRussian"
                placeholder="100"
                data={props.data}
                handleChange={props.handleChange}/>
            <NumberField
                title="Math Test Year (optional)"
                mask="9999"
                label="yearMath"
                placeholder="yyyy"
                data={props.data}
                handleChange={props.handleChange}/>
            <NumberField
                title="Math Test Result (optional)"
                mask="999"
                label="gradeMath"
                placeholder="100"
                data={props.data}
                handleChange={props.handleChange}/>
            <NumberField
                title="Informatics/Physics Test Year (optional)"
                mask="9999"
                label="yearInfoPhysics"
                placeholder="yyyy"
                data={props.data}
                handleChange={props.handleChange}/>
            <NumberField
                title="Informatics/Physics Test Result (optional)"
                mask="999"
                label="gradeInfoPhysics"
                placeholder="100"
                data={props.data}
                handleChange={props.handleChange}/>
            <TextField title="Date of Diploma/School Leaving Certificate Issue (optional)"
                       label="dateDiploma"
                       type="date"
                       data={props.data}
                       handleChange={props.handleChange}/>
            <input className={styles.nextSectionButton}
                   type={"submit"}
                   value={"Save"}/>
        </form>
    );
};


const TextField = (props) => {
    return (
        <div>
            <p className={styles.textFieldTitle}>{props.title}</p>
            <input className={styles.textFieldInput}
                   type={props.type || "text"}
                   placeholder={props.title}
                   value={props.data[props.label] || ""}
                   onChange={props.handleChange(props.label)}/>
        </div>

    );
};

const NumberField = (props) => {
    return (
        <div>
            <p className={styles.textFieldTitle}>{props.title}</p>
            <InputMask className={styles.textFieldInput}
                       mask={props.mask}
                       value={props.data[props.label] || ""}
                       placeholder={props.placeholder || ""}
                       onChange={props.handleChange(props.label)}/>
        </div>
    );
};
