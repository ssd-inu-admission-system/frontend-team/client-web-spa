// Here we export our store

import user from './user/reducer';
import actionCenter from './actionCenter/reducer';
import tests from './tests/reducer';

export {
    user,
    actionCenter,
    tests
};
