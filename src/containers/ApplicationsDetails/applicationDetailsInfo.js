import React from 'react';
import styles from "./applicationsDetails.module.css";
import document from "../../assets/text-document.png";
import {BASE_URL} from "../../services/apiConnector";
import Layout from "../../components/Layout/Layout";

export const PersonalData = (props) => {
    const list = [
        {key: "Email", valueKey: "email"},
        {key: "Phone", valueKey: "phone"},
        {key: "Citizenship", valueKey: "citizenship"},
        {key: "Country of living", valueKey: "country"},
        {key: "City", valueKey: "city"},
        {key: "Source", valueKey: "source"},
    ];

    return (
        <div className={styles.fieldsContainer}>
            <div className={styles.fieldsNames}>
                <div>
                    Full Name:
                </div>
                <div>
                    Date of birth:
                </div>
                <div>
                    Gender:
                </div>
                {list.map((object) => {
                    return (
                        <div key={object.valueKey}>
                            {object.key}:
                        </div>
                    );
                })}
            </div>
            <div>
                <div>
                    {`${props.userData.firstName} 
                      ${props.userData.middleName} 
                      ${props.userData.lastName}`}
                </div>
                <div>
                    {props.userData.birthDate || "-"}
                </div>
                <div>
                    {props.userData.gender ? 'Male' : 'Female'}
                </div>
                {list.map((object) => {
                    return (
                        <div key={object.valueKey}>
                            {props.userData[object.valueKey] || "-"}
                        </div>
                    );
                })}
            </div>
        </div>
    );
};


export const EducationData = (props) => {
    const list = [
        {key: "Country of Study", valueKey: "schoolCountry"},
        {key: "City of Study", valueKey: "schoolCity"},
        {key: "Name of School", valueKey: "schoolName"},
        {key: "Graduation Year (actual or expected)", valueKey: "graduationYear"},
        {key: "Average Grade (optional)", valueKey: "gradeAverage"},
        {key: "Russian Test Year (optional)", valueKey: "yearRussian"},
        {key: "Russian Test Result (optional)", valueKey: "gradeRussian"},
        {key: "Math Test Year (optional)", valueKey: "yearMath"},
        {key: "Math Test Result (optional)", valueKey: "gradeMath"},
        {key: "Informatics/Physics Test Year (optional)", valueKey: "yearInfoPhysics"},
        {key: "Informatics/Physics Test Result (optional)", valueKey: "gradeInfoPhysics"},
        {key: "Date of  Diploma/School Leaving Certificate Issue (optional)", valueKey: "dateDiploma"},
    ];
    console.log(props.userData);
    return (
        <div className={styles.fieldsContainer}>
            <div className={styles.fieldsNames}>
                {list.map((object) => {
                    return (
                        <div key={object.key}>
                            {object.key}:
                        </div>
                    );
                })}
            </div>
            <div>
                {list.map((object) => {
                    return (
                        <div key={object.valueKey}>
                            {props.userData[object.valueKey] || "-"}
                        </div>
                    );
                })}
            </div>
        </div>
    );
};


export const Tests = (props) => {
    return (
        <div>
            {props.currentApplication.tests_info.map((object) => {
                return (
                    <div key={object.test_id}>
                        {object.title}: {object.correct} out of {object.total} correct answers
                    </div>
                );
            })}
        </div>
    );
};

export const Documents = (props) => {
    const document_types = {
        1: 'CV/portfolio',
        2: 'Motivation Letter',
        3: 'Passport Scan',
        4: 'Transcripts',
        5: 'Recommendations',
        6: 'Project Description',
    };
    console.log(props.currentApplication.documents);
    return (
        <div className={styles.documentsContainer}>
            {props.currentApplication.documents.map((object) => {
                return (
                    <div className={styles.documentContainer}
                         onClick={(event) => {
                             event.preventDefault();
                             window.open(BASE_URL + object.document_url, '_blank');
                         }}
                         key={object.document_id}>
                        <div style={{margin: 8}}>
                            {document_types[object.document_type]}
                        </div>
                        <img src={document} alt="document"/>
                        <div style={{margin: 8}}>
                            {object.document_name}
                        </div>
                    </div>
                );
            })}
        </div>
    );
};
