import React from "react";

import logo from "../../assets/university_logo.png";
import styles from "./loginPageStaticData.module.css";

export const StaticData = (props) => {
    return (
        <div className={styles.staticDataContainer}>
            <div className={styles.horizontal}>
                <img className={styles.icon} src={logo} alt={"Logo"}/>
                <p className={styles.universityName}>
                    INU University
                </p>
            </div>
            <p className={styles.text1}>
                Apply for a program today
            </p>
            <div className={styles.line}/>
            <p className={styles.text2}>
                And get education in one of the best universities in the world
            </p>
            <p className={styles.text3}>
                <span className={styles.text3span}>
                By pressing the «Sign up» button you hereby confirm your consent that Innopolis University processes your personal data, according to clause 9 of Federal Law of 27 July 2006 N 152-FZ «On personal data».
                </span>
            </p>
        </div>
    );
};
