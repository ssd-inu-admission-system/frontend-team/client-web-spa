import React from "react";

import styles from "./testTitle.module.css";

export const TestTitle = (props) => {
    return (
        <div>
            <div className={styles.headerInput}>Test Name: </div>
            <span className={styles.title}>{props.title}</span>
            <div className={styles.headerInput}>Description: </div>
            <span className={styles.title}>{props.description || ""}</span>
        </div>
    );
};