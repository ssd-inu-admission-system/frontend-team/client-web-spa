import React from 'react';
import InputMask from 'react-input-mask';

import styles from './interviewerDialog.module.css';
import {connect} from "react-redux";
import {getInterviewers} from "../../../store/staff/reducer";
import {LoadingWidget} from "../../../components/LoadingWIdget/loadingWidget";
import {fetchInterviewersFromServer} from "../../../store/staff/actions";
import {assignInterviewer} from "../../../store/applications/actions";

class InterviewerDialog extends React.Component {

    constructor(props) {
        super(props);
        if (!this.props.interviewers || this.props.interviewers.length === 0) {
            props.dispatch(fetchInterviewersFromServer())
        }
    }

    state = {
        interviewer: "",
        date: "",
        time: "",
    };

    handleChange = (key) => (event) => {
        const newState = {
            ...this.state
        };
        newState[key] = event.target.value;

        this.setState(newState);
    };

    onSubmit = () => {
        if (this.state.interviewer.length !== 0
        //&& this.state.date.length === 10
        //&& this.state.time.length === 5) {
        ){
            this.props.onSubmit();
            this.props.dispatch(assignInterviewer(this.state.interviewer, this.props.applicationId));
        }
    };

    render() {
        if (!this.props.interviewers || this.props.interviewers.length === 0) {
            return (<LoadingWidget/>);
        }
        return (
            <div>
                <div className={styles.formContainer}>
                    <p className={styles.textFieldTitle}>{"Interviewer"}</p>
                    <select
                            value={this.state.interviewer}
                            onChange={this.handleChange("interviewer")}>
                        <option></option>
                        {this.props.interviewers.map((interviewer) => {
                            return (
                                <option value={interviewer.staff_id}
                                        key={interviewer.staff_id}>{interviewer.fullName}</option>);
                        })}
                    </select>
                    {/*
                    <p className={styles.textFieldTitle}>{"Date"}</p>
                    <InputMask className={styles.textFieldInput}
                               mask={"99.99.9999"}
                               value={this.state.date}
                               placeholder={"dd.mm.yyyy"}
                               onChange={this.handleChange("date")}/>
                    <p className={styles.textFieldTitle}>{"Time"}</p>
                    <InputMask className={styles.textFieldInput}
                               mask={"99:99"}
                               value={this.state.time}
                               placeholder={"hh:mm"}
                               onChange={this.handleChange("time")}/>
                               */}
                    <button onClick={this.onSubmit} className={styles.submitButton}>
                        Submit
                    </button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        interviewers: getInterviewers(state)
    }
};

export default connect(mapStateToProps)(InterviewerDialog);