import React from "react";

import styles from "./applicationFormWrapper.module.css";
import Layout from "../Layout/Layout";

export const ApplicationFormWrapper = (props) => {
    return (
        <Layout
          userName={props.userName}
          userStatus={props.userStatus}
        >
            <div className={styles.personalDataContainer}>
                <p className={styles.titleText}>
                    Application Form
                </p>
                <div className={styles.line}/>
                <div className={styles.content}>
                    {props.children}
                </div>
            </div>
        </Layout>
    );
};
