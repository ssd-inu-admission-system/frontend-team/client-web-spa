import React from "react";

import styles from "./applicationWrapper.module.css";

import addIcon from '../../assets/icons/add.svg';

export const ApplicationWrapper = (props) => {

    return (
        <div className={styles.personalDataContainer}>
          <div className={styles.header}>
            <p className={styles.titleText}>
              {props.headLine}
            </p>
            {props.addButtonHandler ? (
              <button onClick={props.addButtonHandler}>
                <img src={addIcon} alt={'add'}/>
              </button>
            ) : ''}
          </div>
          <div className={styles.line}/>
          <div className={styles.content}>
              {props.children}
          </div>
        </div>
    );
};
