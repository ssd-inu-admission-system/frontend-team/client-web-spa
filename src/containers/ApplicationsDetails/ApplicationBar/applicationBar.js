import React from 'react';

import styles from './applicationBar.module.css';
import person from "../../../assets/round1.png";
import {getStatus} from '../../ApplicationsListsPage/applicationsListsPage';
import Modal from "../../../components/Modal/Modal";
import InterviewerDialog from "../InterviewDialog/interviewerDialog";

/*
NEEDED props:
application
userInfo
 */
export class ApplicationBar extends React.Component {

    state = {
        isDialogShown: false,
    };

    showDialog = () => {
        let newState = {
            ...this.state
        };
        newState.isDialogShown = !newState.isDialogShown;
        this.setState(newState);
    };

    render() {
        const hidden = this.props.application.application_status !== 1 || this.props.accountType === 2;
        return (
            <div>
                <div className={styles.container}>
                    <img className={styles.userPhoto} src={person} alt="person"/>
                    <div className={styles.fullName}>
                        {`${this.props.user.first_name} ${this.props.user.last_name}, ${getStatus(this.props.application.application_status)}`}
                    </div>
                    <button onClick={this.showDialog}
                            className={styles.button}
                            hidden={hidden}> {/*check if application can be interviewed*/}
                        Set Interview
                    </button>
                    <select className={styles.selector}
                            value={-1}
                            onChange={this.props.handleStatusChange}>
                        <option value={-1} disabled>Change Status</option>
                        {[0, 1, 2, 3].map((code) => {
                            return (
                                <option value={code} key={code}>{getStatus(code)}</option>);
                        })}
                    </select>
                </div>
                {this.state.isDialogShown ? this.getDialog() : ""}
            </div>

        );
    }

    getDialog() {
        return (
            <Modal onCancelClick={this.showDialog}>
                <InterviewerDialog onSubmit={this.showDialog} applicationId={this.props.applicationId}/>
            </Modal>);
    }
}

