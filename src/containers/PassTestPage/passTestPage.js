import React from 'react';
import {ApplicationWrapper} from '../../components/ApplicationWrapper/applicationWrapper';
import {TestTitle} from '../../components/TestTitle/testTitle';
import {TestQuestionForm} from "../../components/TestQuestionForm/testQuestionForm";
import {connect} from "react-redux";
import {fetchTestInfoFromServer, sendTestSolution} from "../../store/tests/actions";

import styles from './passTestPage.module.css';
import {LoadingWidget} from "../../components/LoadingWIdget/loadingWidget";
import {getTestsInfo} from "../../store/tests/reducer";
import Layout from "../../components/Layout/Layout";
import {getFullName, getUserId} from "../../store/user/reducer";

class PassTestPage extends React.Component {

    constructor(props) {
        super(props);

        this.props.dispatch(fetchTestInfoFromServer(this.props.match.params.id));
    }

    state = {
        test: undefined
    };

    render() {
        if (!this.state.test) {
            return (<LoadingWidget/>);
        }
        return (
            <Layout
                userName={this.props.userName}
                userStatus={"ID: " + this.props.userId}>
                <ApplicationWrapper headLine="Pass test">
                    <TestTitle
                        title={this.state.test.title}
                        description={this.state.test.description}/>
                    {this.state.test.questions.map((question, index) => {
                        return <TestQuestionForm
                            key={question.number}
                            question={question}
                            onQuestionChange={this.onQuestionChangeCreator(index)}/>
                    })}
                    <div>
                        <button onClick={this.submitTest} className={styles.buttonName}>SUBMIT ANSWERS</button>
                    </div>
                </ApplicationWrapper>
            </Layout>
        )
    }

    static getDerivedStateFromProps(props, state) {
        if (typeof state.test === "undefined" && typeof props.test !== "undefined") {
            // put test from props to state
            return {
                ...state,
                test: {
                    ...props.test,
                    description: ""
                },
            };
        } else if (props.test && state.test && props.test.test_id !== state.test.test_id) {
            // update state when new test is loaded (for example going from /edit_test/1 to /edit_test/2)
            return {
                ...state,
                test: {
                    ...props.test,
                    description: ""
                },
            };
        }
        return null;
    }

    onQuestionChangeCreator = (index) => (key, newValue) => {
        let newState = {
            ...this.state
        };
        newState.test.questions[index][key] = newValue;
        this.setState(newState);
    };

    submitTest = () => {
        const result = {
            test_id: Number(this.props.match.params.id),
        };

        result.answers = this.state.test.questions.map((question, index) => {

            const answers = [];
            question.options.forEach((options, index) => {
                if (options.chosen) {
                    answers.push(index+1);
                }
            });
            return {
                number: question.number,
                answers: answers,
            }
        });
        this.props.dispatch(sendTestSolution(result));
        /*
        let questionsCorrect = true;
        if (this.state.test.title.length === 0) {
            this.displayError("title is not correct");
            questionsCorrect = false;
        } else if (this.state.test.description.length === 0) {
            this.displayError("description is not correct");
            questionsCorrect = false;
        }
        this.state.test.questions.forEach((question, index) => {

            let noAnswerChecked = true;
            if (question.text.length === 0) {
                this.displayError(`${index} question is not correct`);
                questionsCorrect = false;
            }
            question.options.forEach((option, optionIndex) => {
                if (option.text.length === 0) {
                    this.displayError(`${index} question, ${optionIndex} option is not correct`);
                    questionsCorrect = false;
                }
                if (option.is_correct) {
                    noAnswerChecked = false;
                }
            });

            if (noAnswerChecked) {
                questionsCorrect = false;
                this.displayError(`${index} question no answer selected`);
            }
        });
        if (questionsCorrect) {
            this.props.dispatch(updateTest(this.props.match.params.id, this.state.test));
        }
        */
    };
}

const mapStateToProps = (state) => {
    return {
        test: getTestsInfo(state),
        userName: getFullName(state),
        userId: getUserId(state),
    }
};

export default connect(mapStateToProps)(PassTestPage);