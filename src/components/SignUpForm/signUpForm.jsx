import React from "react";
import InputMask from 'react-input-mask';

import styles from "./signUpForm.module.css";

export const SignUpForm = (props) => {
    return (
        <form className={styles.formContainer} onSubmit={props.onSubmit}>
            <div className={styles.textButtonContainer}>
                <button type={"button"} className={styles.textButton} onClick={props.onChangeForm}>
                    SIGN IN
                </button>
            </div>
            <p className={styles.signUpText}>Sign up</p>
            <input className={styles.inputTextFiled}
                   type={"text"}
                   placeholder="Name"
                   value={props.data["firstName"] || ""}
                   onChange={props.handleChange("firstName")}/>
            <input className={styles.inputTextFiled}
                   type={"text"}
                   placeholder="Surname"
                   value={props.data["lastName"] || ""}
                   onChange={props.handleChange("lastName")}/>
            <InputMask className={styles.inputTextFiled}
                       mask="9-(999)-999-99-99"
                       placeholder={"Phone"}
                       value={props.data["phone"] || ""}
                       onChange={props.handleChange("phone")}/>
            <input className={styles.inputTextFiled}
                   type={"email"}
                   placeholder="Email"
                   value={props.data["email"] || ""}
                   onChange={props.handleChange("email")}/>
            <input className={styles.inputTextFiled}
                   type={"password"}
                   placeholder="Password"
                   value={props.data["password"] || ""}
                   onChange={props.handleChange("password")}/>
            <input className={styles.inputTextFiled}
                   type={"text"}
                   placeholder="How did you know about us?"
                   value={props.data["source"] || ""}
                   onChange={props.handleChange("source")}/>
            <input className={styles.inputTextFiled}
                   type={"text"}
                   placeholder="Citizenship"
                   value={props.data["citizenship"] || ""}
                   onChange={props.handleChange("citizenship")}/>
            <input className={styles.signUpButton}
                   type={"submit"}
                   value={"SIGN UP"}/>
        </form>
    );
};
