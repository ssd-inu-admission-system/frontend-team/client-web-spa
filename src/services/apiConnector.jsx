// This file is needed to describe all the calls to API. Example is below

import axios from 'axios';

export const BASE_URL = 'http://10.90.138.174:5000';

function checkString(string) {
    return typeof (string) == 'string' &&
    string.trim().length > 0 ?
        string.trim() : false;
}

function checkNumber(number) {
    return typeof (number) == 'number' &&
    number > 0 ?
        number : false;
}

// Implementation of request to /users/sign_up
// Creation of a user
// Required data: object that includes following fields:
// - email
// - password
// - firstName
// - lastName
// - phone
// - citizenship
// - source
export async function signUp(data) {
    const email = checkString(data.email);
    const password_hash = checkString(data.password);
    const first_name = checkString(data.firstName);
    const last_name = checkString(data.lastName);
    const phone = checkString(data.phone);
    const citizenship = checkString(data.citizenship);
    const source = checkString(data.source);

    if (email && password_hash && first_name && last_name
        && phone && citizenship && source) {
        const url = BASE_URL + '/users/sign_up';

        try {
            const response = await axios.post(url, {
                    email,
                    password_hash,
                    first_name,
                    last_name,
                    phone,
                    citizenship,
                    source
                }, {
                    headers: {'Content-Type': 'application/json'}
                }
            );

            if (response.status === 200) {
                return response.data;
            } else {
                return {
                    status: -1,
                    error_message: 'Something went wrong... Server returned code '
                        + response.status
                };
            }
        } catch (e) {
            return {
                status: -1,
                error_message: 'Something went wrong... ' + e
            };
        }
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /applications/{id}/test_results
// Get a list of all tests
// Required data: token, id - most probably user id
export async function getTestsResults(application_id, token) {
    if (token && (typeof application_id !== "undefined")) {
        return await get({token}, `/applications/${application_id}/test_results`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /applications/create
// Get a list of all tests
// Required data: token, applicant_id
export async function createApplication(token, applicant_id) {
    if (token && (typeof applicant_id!== "undefined")) {
        return await postWithParams({applicant_id}, {token}, `/applications/create`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /tests
// Get a list of all tests
// Required data: token
export async function getTests(token) {
    if (token) {
        return await get({token}, `/tests`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /tests/{id}/info
// Get information about a test (not the results)
// Required data: token, id
export async function getTestInfo(token, id) {
    if (token && id) {
        return await get({token, id}, `/tests/${id}/info`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /applications/{id}/info
// Get information about a test (not the results)
// Required data: token, id
export async function getApplicationInfo(token, id) {
    if (token && (typeof id !== "undefined")) {
        return await get({token}, `/applications/${id}/info`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /applications/{id}/send_test
// Send test results for an application
// Required data: testSolutions
export async function sendTestSolutions(testSolutions, applicationId, token) {
    if (testSolutions && (typeof applicationId !== "undefined") && token) {
        return await postWithParams(testSolutions, {token}, `/applications/${applicationId}/send_test`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /users/login
// Get authorization token from server
// Required data: email, password
export async function signIn(email, password) {
    if (email && password) {
        const url = BASE_URL + '/users/login';

        try {
            const response = await axios.post(url, {
                    password_hash: password,
                    email
                }, {
                    headers: {'Content-Type': 'application/json'}
                }
            );

            if (response.status === 200) {
                return response.data;
            } else {
                return {
                    status: -1,
                    error_message: 'Something went wrong... Server returned code '
                        + response.status
                };
            }
        } catch (e) {
            return {
                status: -1,
                error_message: 'Something went wrong... ' + e
            };
        }
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /users/info
// Get information about user
// Required data: token, id
export async function getFullUserInfo(token, id) {
    if (token && id) {
        const url = BASE_URL + `/users/${id}/info`;

        try {
            const response = await axios.get(url, {
                    params: {token},
                    headers: {'Content-Type': 'application/json'}
                }
            );

            if (response.status === 200) {
                return response.data;
            } else {
                return {
                    status: -1,
                    error_message: 'Something went wrong... Server returned code '
                        + response.status
                };
            }
        } catch (e) {
            return {
                status: -1,
                error_message: 'Something went wrong... ' + e
            };
        }
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /users/{id}/update_info
// Update personal info of a user
// Required data: token, id, object that includes following fields:
// “email”: string,
// “firstName”: string,
// “lastName”: string,
// “phone”: string,
// “citizenship”: string,
// “source”: string,
// ”birthDate”: date,
// “gender”: "male"|"female",
// “country”: string,
// “city”: string,
// (optional)”middleName”: string,
export async function updatePersonalInfo(token, id, data) {
    const email = checkString(data.email);
    const first_name = checkString(data.firstName);
    const last_name = checkString(data.lastName);
    const phone = checkString(data.phone);
    const citizenship = checkString(data.citizenship);
    const source = checkString(data.source);
    const birth_date = checkString(data.birthDate);
    const country = checkString(data.country);
    const city = checkString(data.city);
    const gender = checkString(data.gender);

    if (email && first_name && last_name && phone && citizenship
        && source && birth_date && country && city && gender
        && checkString(token) && typeof (id) == "number") {
        const url = BASE_URL + `/users/${id}/update_info`;

        const reqData = {
            email, first_name, last_name,
            phone, citizenship, source,
            birth_date, country, city,
            gender: gender === "male"
        };

        if (checkString(data.middleName)) reqData.middle_name = checkString(data.middleName);

        try {
            const response = await axios.post(url, reqData, {
                    headers: {'Content-Type': 'application/json'},
                    params: {token}
                }
            );

            if (response.status === 200) {
                return response.data;
            } else {
                return {
                    status: -1,
                    error_message: 'Something went wrong... Server returned code '
                        + response.status
                };
            }
        } catch (e) {
            return {
                status: -1,
                error_message: 'Something went wrong... ' + e
            };
        }
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /users/{id}/update_education
// Update information about education
// Required data: token, id, object that includes following fields:
// “schoolCountry”: string,
// “schoolCity”: string,
// “schoolName”: string,
// “graduationYear”: int,
// (optional)”gradeAverage”: float,
// (optional)”yearRussian”: int,
// (optional)”gradeRussian”: int,
// (optional)”yearMath”: int,
// (optional)”gradeMath”: int,
// (optional)”yearInfoPhysics”: int,
// (optional)”gradeInfoPhysics”: int,
// (optional)”dateDiploma”: date
export async function updateEducationInfo(token, id, data) {
    const school_country = checkString(data.schoolCountry);
    const school_city = checkString(data.schoolCity);
    const school_name = checkString(data.schoolName);
    const graduation_year = checkNumber(+data.graduationYear);

    if (school_city && school_country && school_name && graduation_year
        && checkString(token) && typeof (id) == "number") {
        const url = BASE_URL + `/users/${id}/update_education`;

        const reqData = {
            school_country, school_name, school_city, graduation_year,
            grade_average: checkNumber(+data.gradeAverage) ? checkNumber(+data.gradeAverage) : undefined,
            year_russian: checkNumber(+data.yearRussian) ? checkNumber(+data.yearRussian) : undefined,
            grade_russian: checkNumber(+data.gradeRussian) ? checkNumber(+data.gradeRussian) : undefined,
            year_math: checkNumber(+data.yearMath) ? checkNumber(+data.yearMath) : undefined,
            grade_math: checkNumber(+data.gradeMath) ? checkNumber(+data.gradeMath) : undefined,
            year_info_physics: checkNumber(+data.yearInfoPhysics) ? checkNumber(+data.yearInfoPhysics) : undefined,
            grade_info_physics: checkNumber(+data.gradeInfoPhysics) ? checkNumber(+data.gradeInfoPhysics) : undefined,
            date_diploma: checkNumber(+data.dateDiploma) ? checkNumber(+data.dateDiploma) : undefined,
        };

        try {
            const response = await axios.post(url, reqData, {
                    headers: {'Content-Type': 'application/json'},
                    params: {token}
                }
            );

            if (response.status === 200) {
                return response.data;
            } else {
                return {
                    status: -1,
                    error_message: 'Something went wrong... Server returned code '
                        + response.status
                };
            }
        } catch (e) {
            return {
                status: -1,
                error_message: 'Something went wrong... ' + e
            };
        }
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

////////////////////////////////////////////////////////////////////
/// Help functions                                               ///
////////////////////////////////////////////////////////////////////


// url will be formed as BASE_URL + URL
// params is an object with params. OBJECT, this is important
async function get(params, URL) {
    if (params && URL) {
        const url = BASE_URL + URL;

        try {
            const response = await axios.get(url, {
                    params: params,
                    headers: {'Content-Type': 'application/json'}
                }
            );

            if (response.status === 200) {
                return response.data;
            } else {
                return {
                    status: -1,
                    error_message: 'Something went wrong... Server returned code '
                        + response.status
                };
            }
        } catch (e) {
            return {
                status: -1,
                error_message: 'Something went wrong... ' + e
            };
        }
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// data is Object
// url will be formed as BASE_URL + URL
async function post(data, URL) {
    const url = BASE_URL + URL;
    try {
        const response = await axios.post(url, data, {
                headers: {'Content-Type': 'application/json'}
            }
        );

        if (response.status === 200) {
            return response.data;
        } else {
            return {
                status: -1,
                error_message: 'Something went wrong... Server returned code '
                    + response.status
            };
        }
    } catch (e) {
        return {
            status: -1,
            error_message: 'Something went wrong... ' + e
        };
    }
}

// this function is similar to function above
// needed just to be sure
// TODO: may be remove this function
// data is Object
// url will be formed as BASE_URL + URL
// params is an object with params. OBJECT, this is important
async function postWithParams(data, params, URL) {
    const url = BASE_URL + URL;
    try {
        const response = await axios.post(url, data, {
                headers: {'Content-Type': 'application/json'},
                params: params
            }
        );

        if (response.status === 200) {
            return response.data;
        } else {
            return {
                status: -1,
                error_message: 'Something went wrong... Server returned code '
                    + response.status
            };
        }
    } catch (e) {
        return {
            status: -1,
            error_message: 'Something went wrong... ' + e
        };
    }
}
