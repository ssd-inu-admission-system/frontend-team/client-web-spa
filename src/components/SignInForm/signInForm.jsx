import React from "react";

import styles from "./signInForm.module.css";

export const SignInForm = (props) => {
    return (
        <form className={styles.formContainer} onSubmit={props.onSubmit}>
            <div className={styles.textButtonContainer}>
                <button type={"button"} className={styles.textButton} onClick={props.onChangeForm}>
                    SIGN UP
                </button>
            </div>
            <p className={styles.signInText}>Sign In</p>
            <input className={styles.inputTextFiled}
                   type={"email"}
                   placeholder="Email"
                   value={props.data["email"] || ""}
                   onChange={props.handleChange("email")}/>
            <input className={styles.inputTextFiled}
                   type={"password"}
                   placeholder="Password"
                   value={props.data["password"] || ""}
                   onChange={props.handleChange("password")}/>
            <input className={styles.signUpButton}
                   type={"submit"}
                   value={"SIGN IN"}/>
        </form>
    );
};
