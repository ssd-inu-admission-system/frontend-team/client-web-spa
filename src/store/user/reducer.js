import * as types from './actionTypes';

// --------------------------------------------------
//
//  Initial state of this part
//
// --------------------------------------------------

const initialState = {
  token: undefined,
  id: -1,
  data: {
    email: '',
    firstName: '',
    lastName: '',
    phone: '',
    citizenship: '',
    source: '',
    birthDate: '',
    gender: '',
    country: '',
    city: '',
    middleName: '',
    schoolCountry: '',
    schoolCity: '',
    schoolName: '',
    graduationYear: '',
    gradeAverage: '',
    yearRussian: '',
    gradeRussian: '',
    yearMath: '',
    gradeMath: '',
    yearInfoPhysics: '',
    gradeInfoPhysics: '',
    dateDiploma: '',
    applications: []
  },
  applicationData: undefined
};

// --------------------------------------------------
//
//  Reducer for this part
//
// --------------------------------------------------

export default function reduce(state = initialState, action) {
  switch (action.type) {
    case types.TOKEN_FETCHED:
      return {
        ...state,
        token: action.token,
        id: action.id,
        data: {
          ...state.data,
          firstName: action.firstName,
          lastName: action.lastName
        },
      };
    case types.SIGNED_UP:
      return {
        ...state,
        id: action.id,
        token: action.token,
        data: {
          ...state.data,
          ...action.data,
        },
      };
    case types.PERSONAL_INFO_UPDATED:
      return {
        ...state,
        data: {
          ...state.data,
          ...action.data
        }
      };
    case types.EDUCATION_INFO_UPDATED:
      return {
        ...state,
        data: {
          ...state.data,
          ...action.data
        }
      };
    case types.USER_INFO_FETCHED:
      return {
        ...state,
        data: {
          ...state.data,
          ...action.data
        }
      };
    case types.APPLICATION_CREATED:
      return {
        ...state,
        data: {
          ...state.data,
          applications: [...state.data.applications, action.application_id]
        }
      };
    case types.TOKEN_INVALIDATED:
      return {
        ...state,
        token: undefined,
        id: -1
      };
    case types.APPLICATION_FETCHED:
      return {
        ...state,
        applicationData: action.applicationData
      };
    default:
      return state;
  }
}

// --------------------------------------------------
//
//  Selectors for this part
//
// --------------------------------------------------

export function getUserToken(state) {
  return state.user.token;
}

export function getUserId(state) {
  return state.user.id;
}

export function getFullName(state) {
  return state.user.data.firstName + ' ' + state.user.data.lastName;
}

export function getUserData(state) {
  return state.user.data;
}

// returns first application id from the list
export function getApplicationID(state) {
  return state.user.data.applications[0];
}

export function getApplicationData(state) {
  return state.user.applicationData;
}

export function getApplicationStatus(state) {
  if (!state.user.applicationData) {
    return undefined;
  }
  return state.user.applicationData.application_status;
}

export function getDocuments(state) {
  return state.user.applicationData ? state.user.applicationData.documents : null;
}
